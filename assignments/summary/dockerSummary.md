## Docker Summary

![Docker Logo](https://www.docker.com/sites/default/files/social/docker_facebook_share.png)

### Docker overview
Docker is an open platform for developing, shipping, and running applications. 

1. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. 
2. With Docker, you can manage your infrastructure in the same ways you manage your applications. 
3. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

### What is the use of Docker?
1. Environment Standardization
2. Build once, deploy anywhere - Build once on master 
3. Isolation - Work on separate dockers for separate applications
4. Portability

#### Why is docker better than simple virtual machines?
* In VMs, if resource are by the allocated then it cannot be reallocated. That is, if there are 4 VMs and if 3 are performing heavy duty work then the resources from the 4th VM cannot be reallocated to the 3 VMs.  
* On the other hand, Dockers share the resources of the master node.

#### Docker with ML/DL
1. Create a new conda environment before starting a new project
2. Complete the project
3. Dockerize the conda environment
4. Now anyone can use this docker environment on their machine without having to install any dependencies.
