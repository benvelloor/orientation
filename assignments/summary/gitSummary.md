## Git Summary

![Git Logo](https://camo.githubusercontent.com/b75d955466c5f5602998b752dd97ff1bdbe16168/68747470733a2f2f6769742d73636d2e636f6d2f696d616765732f6c6f676f732f646f776e6c6f6164732f4769742d4c6f676f2d32436f6c6f722e706e67)

### What is Git?
* Git is version-control software that makes collaboration with teammates super simple.   
* Git lets you easily keep track of every revision you and your team make during the development of your software.

### Git Overview

#### The Three Stages
1. Modified means that you have changed the file but have not committed it to your database yet.
2. Staged means that you have marked a modified file in its current version to go into your next commit snapshot.
3. Committed means that the data is safely stored in your local database.

#### The Four Main Sections of a Git project:
1. Working Directory : All the changes you make via Editor(s) is done in this tree of repository.
2. Staging Area : All the staged files go into this tree of your repository.
3. Local Repository : All the committed files go to this tree of your repository.
4. Remote Repository : This is the copy of your Local Repository but is stored in some server on the Internet.

![Git Basic Workflow](https://res.cloudinary.com/practicaldev/image/fetch/s--M_fHUEqA--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/128hsgntnsu9bww0y8sz.png)

### Git Workflow

1. Clone the repo  
```
$ git clone <link-to-repository> 
```
3. Pull the remote repo  
```
$ git pull 
```
3. Create a new branch  
```
$ git checkout master  
$ git checkout -b <your-branch-name>
```
4. Modify the files using an Editor

5. Stage the files that you want in the want in the next commit
```
$ git add .         #(.) adds all files   
```
6. Commit the staged files  
```
$ git commit -m "initial commit"  #(-m "") The text inside the quotes becomes the name of the commit
```
7. Push the commit onto the remote repo 
```
$ git push
```
